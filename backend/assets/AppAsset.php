<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
		'plugins/datatables/dataTables.bootstrap.css',
		'plugins/select2/select2.min.css',
    ];
    public $js = [
	'plugins/datatables/jquery.dataTables.min.js',
	'plugins/datatables/dataTables.bootstrap.min.js',
	'plugins/select2/select2.full.min.js',
	'plugins/js/modal.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',
		
    ];
}
