
<link rel="shortcut icon" href="<?php // echo Yii::$app->request->baseUrl; ?>/image/bps.png" type="image/x-icon" />
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
	<div class="box box-solid box-success">
		<div class="box-header">
			<h3 style="text-align: center">
				<b>OFFICE SYSTEM</b> 
			</h3>
		 
			
		</div><!-- /.box-header -->
		<div class="login-box-body" style="background : #00a65a">
		<p class="login-box-msg"> </p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Masuk', ['class' => 'btn btn-block btn-success', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>

		
        <?php Yii::$app->session->set('unique_code', 'xxxx'); ActiveForm::end(); ?>
		

        <!-- <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in
                using Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign
                in using Google+</a>
        </div> -->
        <!-- /.social-auth-links -->
		
        <a style="color: white">Lupa password?</a><br>
		<a style="color: white">Hubungi Admin</a>
		</div>
    <!-- /.login-logo -->
    
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
