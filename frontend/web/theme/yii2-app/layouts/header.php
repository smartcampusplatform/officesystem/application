<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
<?php 
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){
			$user=$data['username'];

			$role=$data['role'];
					}
?>


    <?= Html::a('<span class="logo-mini"></span><span class="logo-lg">SOFFICE</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
		<div id="navbar-collapse" class="collapse navbar-collapse pull-left">
			
		</div>
        <div class="navbar-custom-menu">
		
            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->
            
                

                
			<li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">2</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Kamu memiliki 2 notifikasi.</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> Anda di undang rapat mingguan tanggal 17/05/2019
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> Anda di undang rapat Proyek tanggal 15/05/2019
                    </a>
                  </li>
                 
                  
                  
                </ul>
              </li>
              <li class="footer"><a href="#">Lihat Detail</a></li>
            </ul>
          </li>
		  
		  <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/avatar.png" class="user-image" alt="User Image"/>
                        <span ><?php echo $user; ?> </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/avatar.png" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?php echo $role; ?> 
                                <small><?php echo $role; ?> </small>
                            </p>
                        </li>
                        <!-- Menu Body 
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </li> -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Log out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
			
                <!-- User Account: style can be found in dropdown.less 
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li> -->
            </ul>
        </div>
    </nav>
</header>
