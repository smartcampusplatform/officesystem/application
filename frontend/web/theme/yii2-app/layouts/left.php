<aside class="main-sidebar">
<?php 
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){
			$user=$data['username'];
			
			$role=$data['role'];
					}
?>
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <!--<div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Fahrur Rozi</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> -->

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
		<div class="prettyprint">
        <?= dmstr\widgets\Menu::widget(
			
					[
						'options' => ['class' => 'sidebar-menu'],
						'items' => [
							['label' => 'Menu Utama', 'options' => ['class' => 'header']],
							['label' => 'Home', 'icon' => 'fa fa-fw fa-home', 'url' => ['/site/index']],
							[
								'label' => 'Kelola Master', 
								'icon' => 'fa fa-fw fa-database', 
								'url' => '#',
								'items' => [
									['label' => 'Master Signature', 'icon' => 'fa fa-fw fa-circle-o', 'url' => '#',],	
									['label' => 'Master Tipe Dokumen', 'icon' => 'fa fa-fw fa-circle-o', 'url' => '#',],									
									],
								'visible' => ($role=='admin'),
								],							
								[
								'label' => 'Dokumen Masuk', 'icon' => 'fa fa-fw  fa-arrow-circle-down', 'url' =>  ['/document/index'],
								'items' => [	
										
									]						
								],
								
								[								
								'label' => 'Dokumen Keluar', 'icon' => 'fa fa-fw fa-arrow-circle-up', 'url' =>  '#',
								'items' => [	
									['label' => 'Draft', 'icon' => 'fa fa-fw fa-circle-o', 'url' =>['/document/index'],],
									['label' => 'Terkirim', 'icon' => 'fa fa-fw fa-circle-o', 'url' => '#',],
									['label' => 'Buat Surat Keputusan', 'icon' => 'fa fa-fw fa-circle-o', 'url' => ['/document/create'],],
									['label' => 'Buat Surat Biasa', 'icon' => 'fa fa-fw fa-circle-o', 'url' => ['/document/create'],],
									['label' => 'Buat Surat Undangan', 'icon' => 'fa fa-fw fa-circle-o', 'url' => ['/document/create'],],
									
									]							
								],
								[								
								'label' => 'Disposisi', 'icon' => 'fa fa-fw fa-arrow-circle-right ', 'url' =>  ['/disposition/index'],
								'items' => [					
									]							
								],
								[								
								'label' => 'Rapat', 'icon' => 'fa fa-fw fa-file-text', 'url' =>  '#',
								'items' => [	
									['label' => 'Buat Rapat', 'icon' => 'fa fa-fw fa-circle-o', 'url' => ['/meeting/create'],],
									['label' => 'Daftar Rapat', 'icon' => 'fa fa-fw fa-circle-o', 'url' => ['/meeting/daftarrapat'],],															
									
									]							
								],
								[								
								'label' => 'Tugas', 'icon' => 'fa fa-fw fa-file-text-o', 'url' =>  '#',
								'items' => [	
									['label' => 'Buat Tugas', 'icon' => 'fa fa-fw fa-circle-o', 'url' => ['/assignment/create'],],
									['label' => 'Daftar Tugas', 'icon' => 'fa fa-fw fa-circle-o', 'url' => ['/assignment/index'],],
									['label' => 'Tugas Selesai', 'icon' => 'fa fa-fw fa-circle-o', 'url' => '#',],							
									
									]							
								],
								[								
								'label' => 'Arsip', 'icon' => 'fa fa-fw fa-save ', 'url' =>['/archive/index'],
								'items' => [	
																		
									
									
									]							
								],
								
								
								/* [
								'label' => 'Laporan Akses', 'icon' => 'fa fa-fw fa-file', 'url' =>  ['/tuser/laporanakses'],
								//'visible' => ($role=='superadmin'),								
								], */
								[
								'label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],
								'visible' => ($role=='admin'),	],  
						],
					]
					
        )  ?>
		 </div>
		<!-- Sidebar user panel -->
		<!--
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Fahrur Rozi</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> 
		 -->
    </section>

</aside>
