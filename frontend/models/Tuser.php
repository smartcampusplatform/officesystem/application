<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tuser".
 *
 * @property integer $id_tuser
 * @property string $username
 * @property string $updated_at
 */
class Tuser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tuser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'updated_at'], 'required'],
            [['updated_at'], 'safe'],
            [['username'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tuser' => 'Id Tuser',
            'username' => 'Username',
            'updated_at' => 'Updated At',
        ];
    }
}
