<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Document;

/**
 * DocumentSearch represents the model behind the search form of `app\models\Document`.
 */
class DocumentSearch extends Document
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_document', 'sender', 'type', 'id_signature', 'reviewer', 'konseptor'], 'integer'],
            [['no_document', 'date', 'content', 'subject'], 'safe'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Document::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_document' => $this->id_document,
            'date' => $this->date,
            'sender' => $this->sender,
            'type' => $this->type,
            'id_signature' => $this->id_signature,
            'status' => $this->status,
            'reviewer' => $this->reviewer,
            'konseptor' => $this->konseptor,
        ]);

        $query->andFilterWhere(['ilike', 'no_document', $this->no_document])
            ->andFilterWhere(['ilike', 'content', $this->content])
            ->andFilterWhere(['ilike', 'subject', $this->subject]);

        return $dataProvider;
    }
}
