<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meeting".
 *
 * @property int $id_meeting
 * @property string $name
 * @property string $leader
 * @property string $location
 * @property int $attendee
 * @property string $date
 * @property string $time
 * @property string $notulen
 * @property string $notulis
 */
class Meeting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meeting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_meeting'], 'required'],
            [['id_meeting', 'attendee'], 'default', 'value' => null],
            [['id_meeting', 'attendee'], 'integer'],
            [['date', 'time'], 'safe'],
            [['notulen'], 'string'],
            [['name', 'leader', 'location', 'notulis'], 'string', 'max' => 25],
            [['id_meeting'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_meeting' => 'Id Meeting',
            'name' => 'Name',
            'leader' => 'Leader',
            'location' => 'Location',
            'attendee' => 'Attendee',
            'date' => 'Date',
            'time' => 'Time',
            'notulen' => 'Notulen',
            'notulis' => 'Notulis',
        ];
    }
}
