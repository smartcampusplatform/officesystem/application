<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "disposition".
 *
 * @property int $id_disposition
 * @property int $id_document
 * @property int $sender
 * @property int $receiver
 * @property string $date
 * @property string $time
 * @property string $note
 */
class Disposition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'disposition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_disposition'], 'required'],
            [['id_disposition', 'id_document', 'sender', 'receiver'], 'default', 'value' => null],
            [['id_disposition', 'id_document', 'sender', 'receiver'], 'integer'],
            [['date', 'time'], 'safe'],
            [['note'], 'string', 'max' => 50],
            [['id_disposition'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_disposition' => 'Id Disposition',
            'id_document' => 'Id Document',
            'sender' => 'Sender',
            'receiver' => 'Receiver',
            'date' => 'Date',
            'time' => 'Time',
            'note' => 'Note',
        ];
    }
}
