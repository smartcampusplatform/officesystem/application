<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "archive".
 *
 * @property int $id_archive
 * @property int $id_document
 * @property int $archiver
 * @property string $date
 * @property string $time
 * @property bool $status
 */
class Archive extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'archive';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_archive'], 'required'],
            [['id_archive', 'id_document', 'archiver'], 'default', 'value' => null],
            [['id_archive', 'id_document', 'archiver'], 'integer'],
            [['date', 'time'], 'safe'],
            [['status'], 'boolean'],
            [['id_archive'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_archive' => 'Id Archive',
            'id_document' => 'Id Document',
            'archiver' => 'Archiver',
            'date' => 'Date',
            'time' => 'Time',
            'status' => 'Status',
        ];
    }
}
