<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property int $id_document
 * @property string $no_document
 * @property string $date
 * @property int $sender
 * @property int $type
 * @property string $content
 * @property int $id_signature
 * @property bool $status
 * @property int $reviewer
 * @property int $konseptor
 * @property string $subject
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_document'], 'required'],
            [['id_document', 'sender', 'type', 'id_signature', 'reviewer', 'konseptor'], 'default', 'value' => null],
            [['id_document', 'sender', 'type', 'id_signature', 'reviewer', 'konseptor'], 'integer'],
            [['date'], 'safe'],
            [['content'], 'string'],
            [['status'], 'boolean'],
            [['no_document'], 'string', 'max' => 30],
            [['subject'], 'string', 'max' => 100],
            [['id_document'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_document' => 'Id Document',
            'no_document' => 'No Document',
            'date' => 'Date',
            'sender' => 'Sender',
            'type' => 'Type',
            'content' => 'Content',
            'id_signature' => 'Id Signature',
            'status' => 'Status',
            'reviewer' => 'Reviewer',
            'konseptor' => 'Konseptor',
            'subject' => 'Subject',
        ];
    }
}
