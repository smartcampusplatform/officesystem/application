<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "assignment".
 *
 * @property int $id_assignment
 * @property string $name
 * @property int $id_employee
 * @property string $date_start
 * @property string $date_end
 * @property int $realisation
 * @property bool $status
 * @property string $file
 */
class Assignment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'assignment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_assignment'], 'required'],
            [['id_assignment', 'id_employee', 'realisation'], 'default', 'value' => null],
            [['id_assignment', 'id_employee', 'realisation'], 'integer'],
            [['date_start', 'date_end'], 'safe'],
            [['status'], 'boolean'],
            [['file'], 'string'],
            [['name'], 'string', 'max' => 25],
            [['id_assignment'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_assignment' => 'Id Assignment',
            'name' => 'Name',
            'id_employee' => 'Id Employee',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'realisation' => 'Realisation',
            'status' => 'Status',
            'file' => 'File',
        ];
    }
}
