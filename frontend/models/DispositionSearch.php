<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Disposition;

/**
 * DispositionSearch represents the model behind the search form of `app\models\Disposition`.
 */
class DispositionSearch extends Disposition
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_disposition', 'id_document', 'sender', 'receiver'], 'integer'],
            [['date', 'time', 'note'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Disposition::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_disposition' => $this->id_disposition,
            'id_document' => $this->id_document,
            'sender' => $this->sender,
            'receiver' => $this->receiver,
            'date' => $this->date,
            'time' => $this->time,
        ]);

        $query->andFilterWhere(['ilike', 'note', $this->note]);

        return $dataProvider;
    }
}
