<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\helpers\ArrayHelper;
use yii\db\Query;
//use frontend\controllers\DateTime;
use yii\helpers\Time;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','signup'],
                'rules' => [
					
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
					[
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {	
		  if (!Yii::$app->user->isGuest) {
			
			return $this->render('index',[
					/* 'sisahari' => $sisahari,
					'telahberjalan' => $telahberjalan,
					'dataupload'=>$dataupload,
					'user'=>$user,
					'tglupload'=>$tglupload,
					'namakab'=>$namakab,
					'belumkonfirmperkab'=>$belumkonfirmperkab,
					'sudahkonfirmperkab'=>$sudahkonfirmperkab,
					'approvalpropperkab'=>$approvalpropperkab,
					'perbaikanpropperkab'=>$perbaikanpropperkab,
					'jumlah_belum_konfirmasi'=>$jumlah_belum_konfirmasi,
					'jumlah_sudah_konfirmasi'=>$jumlah_sudah_konfirmasi,
					'jumlah_approval_propinsi'=>$jumlah_approval_propinsi,
					'jumlah_sudah_diperbaiki_propinsi'=>$jumlah_sudah_diperbaiki_propinsi */
					
					]);
        }
		/* if (!Yii::$app->user->isGuest) {
			return $this->render('index');
		} */
		else { 
				$model = new LoginForm();
				if ($model->load(Yii::$app->request->post()) && $model->login()) {
				
					return $this->goBack();
				} else {
					return $this->redirect(['site/login', 
						'model' => $model,
					]);
				}
			
		 } 
		
    }
	
	public function actionIndexold()
    {	
		 if (!Yii::$app->user->isGuest) {
		 
			/* chart perhari*/
			$offset=0;
			$sql_count = "SELECT count(DISTINCT DATE(tanggal)) FROM tharga";			
			$count = Yii::$app->db->createCommand($sql_count)->queryScalar();
			if ($count>10)
				$offset=$count-10;
			$sqltanggal = Yii::$app->db->createCommand('SELECT DISTINCT DATE(tanggal) AS tanggal FROM tharga ORDER BY tanggal LIMIT 10 OFFSET '.$offset.' ');
			
			$resulttanggal = $sqltanggal->queryAll();
			$tanggal =[];
			foreach($resulttanggal as $data){
					$tanggal[] = $data['tanggal'];
				}
			
			$sqlkdkualitas = Yii::$app->db->createCommand('SELECT mkualitasmerk.kd_kualitas, mkualitasmerk.kualitas_merk, mkomoditas.nm_komoditas from mkualitasmerk, mkomoditas WHERE mkualitasmerk.kd_komoditas=mkomoditas.kd_komoditas and mkualitasmerk.tampil=1');
			
			$kdkualitas = $sqlkdkualitas->queryAll();
			$seriespertanggal =[];
			foreach($kdkualitas as $kd_kualitas){
					$count = Yii::$app->db->createCommand('SELECT count(DISTINCT DATE(tanggal)) FROM tharga WHERE kd_kualitas='.$kd_kualitas['kd_kualitas'].'')->queryScalar();
					$offset=0;
					if ($count>10)
						$offset=$count-10;
					$sqldata1 = Yii::$app->db->createCommand('SELECT AVG(harga) as harga_rerata FROM tharga WHERE kd_kualitas='.$kd_kualitas['kd_kualitas'].' GROUP BY DATE(tanggal) ORDER BY tanggal LIMIT 10 OFFSET '.$offset.' ');
					
					$resultdata1 = $sqldata1->queryColumn();
					$data=array_map('intval',$resultdata1);
					$seriespertanggal[] = [
						//'name'=>($kd_kualitas['nm_komoditas']),
						'name'=>($kd_kualitas['nm_komoditas']." ".$kd_kualitas['kualitas_merk']),
						'data'=>$data,
					];
				}

			
			/* chart perjam */
			
			$offset=0;
			$sql_count = "SELECT count(DISTINCT DATE(tanggal), HOUR(tanggal)) FROM tharga";			
			$count = Yii::$app->db->createCommand($sql_count)->queryScalar();
			if ($count>10)
				$offset=$count-10;
			$sqljam = Yii::$app->db->createCommand('SELECT DISTINCT DATE(tanggal) AS tanggal, HOUR(tanggal) AS jam FROM tharga ORDER BY tanggal LIMIT 10 OFFSET '.$offset.' ');
			
			$resultjam = $sqljam->queryAll();
			$jam =[];
			foreach($resultjam as $data){
					$jam[] = $data['tanggal']." Pukul ".$data['jam'];
				}
			
			$seriesperjam =[];
			foreach($kdkualitas as $kd_kualitas){
							
					$count = Yii::$app->db->createCommand('SELECT count(DISTINCT DATE(tanggal), HOUR(tanggal)) FROM tharga WHERE kd_kualitas='.$kd_kualitas['kd_kualitas'].'')->queryScalar();
					$offset=0;
					if ($count>10)
						$offset=$count-10;
					$sqldata1 = Yii::$app->db->createCommand('SELECT AVG(harga) as harga_rerata FROM tharga WHERE kd_kualitas='.$kd_kualitas['kd_kualitas'].' GROUP BY DATE(tanggal), HOUR(tanggal) ORDER BY tanggal LIMIT 10 OFFSET '.$offset.' '); 
					
					$resultdata1 = $sqldata1->queryColumn();
					$data=array_map('intval',$resultdata1);
					$seriesperjam[] = [
						//'name'=>($kd_kualitas['nm_komoditas']),
						'name'=>($kd_kualitas['nm_komoditas']." ".$kd_kualitas['kualitas_merk']),
						'data'=>$data,
					];
				}
			
			/* chart perbulan*/
			
			$offset=0;
			$sql_count = "SELECT count(DISTINCT YEAR(tanggal), MONTH(tanggal)) FROM tharga";			
			$count = Yii::$app->db->createCommand($sql_count)->queryScalar();
			if ($count>10)
				$offset=$count-10;
			$sqlbulan = Yii::$app->db->createCommand('SELECT DISTINCT YEAR(tanggal) AS tahun, MONTHNAME(tanggal) AS bulan FROM tharga ORDER BY tanggal LIMIT 10 OFFSET '.$offset.' ');
			
			$resultbulan = $sqlbulan->queryAll();
			$bulan =[];
			foreach($resultbulan as $data){
					$bulan[] = $data['bulan']." ".$data['tahun'];
				}
			
			$seriesperbulan =[];
			$i=0;
			foreach($kdkualitas as $kd_kualitas){
					
					$count = Yii::$app->db->createCommand('SELECT count(DISTINCT YEAR(tanggal), MONTH(tanggal)) FROM tharga WHERE kd_kualitas='.$kd_kualitas['kd_kualitas'].'  ')->queryScalar();
					$offset=0;
					if ($count>10)
						$offset=$count-10;
					$sqldata1 = Yii::$app->db->createCommand('SELECT AVG(harga) as harga_rerata FROM tharga WHERE kd_kualitas='.$kd_kualitas['kd_kualitas'].' GROUP BY YEAR(tanggal), MONTH(tanggal) ORDER BY tanggal LIMIT 10 OFFSET '.$offset.' '); 
					
					$resultdata1 = $sqldata1->queryColumn();
					$data=array_map('intval',$resultdata1);
					$seriesperbulan[] = [
						//'name'=>($kd_kualitas['nm_komoditas']),
						'name'=>($kd_kualitas['nm_komoditas']." ".$kd_kualitas['kualitas_merk']),
						'data'=>$data,
					];
					
				}
			
			/*
			$sqldata1 = Yii::$app->db->createCommand('SELECT kd_kualitas, AVG(harga) as harga_rerata, tanggal FROM tharga WHERE kd_kualitas="109034003"	GROUP BY YEAR(tanggal), MONTH(tanggal), DAY(tanggal) ORDER BY tanggal LIMIT 7 OFFSET '.$offset.' ');
			
			$resultdata1 = $sqldata1->queryAll();
			$sirupabc =[];
			foreach($resultdata1 as $dataharga1){
					$sirupabc[] = (float)$dataharga1['harga_rerata'];
				}
				
			$sqldata2 = Yii::$app->db->createCommand('SELECT kd_kualitas, AVG(harga) as harga_rerata, tanggal FROM tharga WHERE kd_kualitas="109034012"	GROUP BY YEAR(tanggal), MONTH(tanggal), DAY(tanggal) ORDER BY tanggal LIMIT 7 OFFSET '.$offset.' ');
			
			$resultdata2 = $sqldata2->queryAll();
			$indofood =[];
			foreach($resultdata2 as $dataharga2){
					$indofood[] = (float)$dataharga2['harga_rerata'];
				}
			
			$sqldata3 = Yii::$app->db->createCommand('SELECT kd_kualitas, AVG(harga) as harga_rerata, tanggal FROM tharga WHERE kd_kualitas="109041003"	GROUP BY YEAR(tanggal), MONTH(tanggal), DAY(tanggal) ORDER BY tanggal LIMIT 7 OFFSET '.$offset.' ');
			
			$resultdata3 = $sqldata3->queryAll();
			$sirup =[];
			foreach($resultdata3 as $dataharga3){
					$sirup[] = (float)$dataharga3['harga_rerata'];
				}
			
            return $this->render('index',[
					'tanggal'=>$tanggal,
					'sirupabc'=>$sirupabc,
					'indofood'=>$indofood,
					'sirup'=>$sirup,
				]);
			*/		
			return $this->render('index',[
					'tanggal'=>$tanggal,
					'jam'=>$jam,
					'bulan'=>$bulan,
					'seriespertanggal'=>$seriespertanggal,
					'seriesperjam'=>$seriesperjam,
					'seriesperbulan'=>$seriesperbulan,
				]);
        }
		else {
				$model = new LoginForm();
				if ($model->load(Yii::$app->request->post()) && $model->login()) {
					return $this->goBack();
				} else {
					return $this->redirect(['site/login', 
						'model' => $model,
					]);
				}
			
		}
		
    }
	
	public function actionCurl()
    {	
		
			return $this->render('curl');
			
		
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        //if (!Yii::$app->user->isGuest) {
        //    return $this->goHome();
        //}

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

       // return $this->goHome();
	   $model = new LoginForm();
		return $this->redirect(['site/login', 
						'model' => $model,
			]);
        
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionAutoscraping()
    {
          return $this->render('autoscraping');
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            
                    return $this->goHome();
				// return $this->redirect(['view', 'id' => $model2->id]);
                
            
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
