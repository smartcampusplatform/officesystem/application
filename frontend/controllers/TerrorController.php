<?php

namespace frontend\controllers;

use Yii;
use app\models\Terror;
use app\models\TerrorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TerrorController implements the CRUD actions for Terror model.
 */
class TerrorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Terror models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionUc()
    {
        $searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('uc', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionError()
    {	
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){$kab=(string)$data['wilayah'];}
		
        $searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		if($kab=='00'){
			$dataProvider->query->where('terror.flag =1 ');}
		ELSE $dataProvider->query->where('terror.flag =1 and terror.kode_kab=\''.$kab.'\' ');

        return $this->render('error', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionKonfirmasi()
    {	
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){$kab=(string)$data['wilayah'];}
        $searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		if($kab=='00'){
			$dataProvider->query->where('terror.flag =2  ');}
		ELSE $dataProvider->query->where('terror.flag =2 and terror.kode_kab=\''.$kab.'\' ');

        return $this->render('konfirmasi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionApproval()
    {	
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){$kab=(string)$data['wilayah'];}
        $searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		if($kab=='00'){
			$dataProvider->query->where('terror.flag = 3  ');}
		ELSE $dataProvider->query->where('terror.flag = 3 and terror.kode_kab=\''.$kab.'\' ');

        return $this->render('approval', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionPerbaikan()
    {	
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){$kab=(string)$data['wilayah'];}
        $searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		if($kab=='00'){
			$dataProvider->query->where('terror.flag = 4 or terror.flag = 5 or terror.flag = 6');}
		ELSE $dataProvider->query->where('(terror.flag = 4 or terror.flag = 5 or terror.flag = 6) and terror.kode_kab=\''.$kab.'\' ');

        return $this->render('perbaikan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    	public function actionApp()
    {	
		
        $searchModel = new TerrorSearch();
       
        return $this->render('app', [
            'searchModel' => $searchModel,
           
        ]);
    }
	
	public function actionDownload($id)
    {
        
		$filename = ' '.Date('Ymd').'-daftar-error-.xls';
		//header("Content-type: application/vnd-ms-excel");
		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		//Response.ContentType ="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

		header("Content-Disposition: attachment; filename=".$filename);
		
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){$kab=$data['wilayah'];}
		
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){
			$wil=$data['wilayah'];
			$user=$data['username'];}
		$no=1;
		/* Excel.Application excel = new Excel.Application();
		Excel.Workbook wb = excel.Application.Workbooks.Add(true);
		Excel.Worksheet excelSheet = (Excel.Worksheet)excel.ActiveSheet; */

    if ($id==1){
		$newtbl="<table border=\"1\" width=\"100%\">";
					$newtbl.="<thead><tr >";
						$newtbl.="<th>ID</th>";
						$newtbl.="<th>Provinsi</th>";
						$newtbl.="<th>Kabupaten</th>";
						$newtbl.="<th>Kecamatan</th>";
						$newtbl.="<th>Desa</th>";
						$newtbl.="<th>NBS</th>";
						$newtbl.="<th>NKS</th>";
						$newtbl.="<th>NURT</th>";
						$newtbl.="<th>Nama KRT</th>";
						
						$newtbl.="<th>Error</th>";
						$newtbl.="<th>Konfirmasi</th>";
						$newtbl.="<th>Catatan</th>";						
						
						
										
					$newtbl.="</tr></thead>";
				$newtbl.="<tbody>";

		if ($kab=='00'){
			
				$query=Yii::$app->db->createCommand(" SELECT * FROM terror WHERE flag=".$id."  order by kode_prop, kode_kab, kode_kec, kode_desa, nbs, no_urut_ruta");

				$result=$query->queryAll();  

				foreach($result as $data)
					{
						$newtbl.="<tr >";
						
						$newtbl.="<td>".$data['id_terror']."</td>";
						$newtbl.="<td>".(string)$data['kode_prop']."</td>";
						$newtbl.="<td>".(string)$data['kode_kab']."</td>";
						$newtbl.="<td>".(string)$data['kode_kec']."</td>";
						$newtbl.="<td>".(string)$data['kode_desa']."</td>";
						$newtbl.="<td>".$data['nbs']."</td>";
						$newtbl.="<td>".$data['nks']."</td>";
						$newtbl.="<td>".$data['no_urut_ruta']."</td>";
						$newtbl.="<td>".$data['nama_krt']."</td>";
						
						$newtbl.="<td>".$data['error']."</td>";
						$newtbl.="<td>".$data['konfirmasi']."</td>";
						$newtbl.="<td>".$data['keterangan']."</td>";
						
						
						$newtbl.="</tr>";
						$no++;
					}
				
		}
		else {
			$query=Yii::$app->db->createCommand(" SELECT * FROM terror WHERE flag=".$id."  and kode_kab='".$kab."'  order by kode_prop, kode_kab, kode_kec, kode_desa, nbs, no_urut_ruta ");

				$result=$query->queryAll();  

				foreach($result as $data)
					{
						$newtbl.="<tr >";
						
						$newtbl.="<td>".$data['id_terror']."</td>";
						$newtbl.="<td>".$data['kode_prop']."</td>";
						$newtbl.="<td>".(string)$data['kode_kab']."</td>";
						$newtbl.="<td>".(string)$data['kode_kec']."</td>";
						$newtbl.="<td>".(string)$data['kode_desa']."</td>";
						$newtbl.="<td>".$data['nbs']."</td>";
						$newtbl.="<td>".$data['nks']."</td>";
						$newtbl.="<td>".$data['no_urut_ruta']."</td>";
						$newtbl.="<td>".$data['nama_krt']."</td>";
						
						$newtbl.="<td>".$data['error']."</td>";
						$newtbl.="<td>".$data['konfirmasi']."</td>";
						$newtbl.="<td>".$data['keterangan']."</td>";
						
						
						$newtbl.="</tr>";
						$no++;
					}
			}
		Yii::$app->db->createCommand("INSERT INTO tdownload (kode_kab, kode_tabel_download, user, islengkap, jumlah_baris, updated_at) VALUES ( '".$wil."' ,4,'".$user."' ,1, '".$no."' , NOW())")->execute();	
    }
    else if ($id==2){
		$newtbl="<table border=\"1\" width=\"100%\">";
					$newtbl.="<thead><tr >";
						$newtbl.="<th>ID</th>";
						$newtbl.="<th>Provinsi</th>";
						$newtbl.="<th>Kabupaten</th>";
						$newtbl.="<th>Kecamatan</th>";
						$newtbl.="<th>Desa</th>";
						$newtbl.="<th>NBS</th>";
						$newtbl.="<th>NKS</th>";
						$newtbl.="<th>NURT</th>";
						$newtbl.="<th>Nama KRT</th>";
						
						$newtbl.="<th>Error</th>";
						$newtbl.="<th>Konfirmasi</th>";
						$newtbl.="<th>Flag</th>";
						$newtbl.="<th>Catatan</th>";						
						
						
										
					$newtbl.="</tr></thead>";
				$newtbl.="<tbody>";

		if ($kab=='00'){
			
				$query=Yii::$app->db->createCommand(" SELECT * FROM terror WHERE flag=".$id."  order by kode_prop, kode_kab, kode_kec, kode_desa, nbs, no_urut_ruta ");

				$result=$query->queryAll();  

				foreach($result as $data)
					{
						$newtbl.="<tr >";
						
						$newtbl.="<td>".$data['id_terror']."</td>";
						$newtbl.="<td>".(string)$data['kode_prop']."</td>";
						$newtbl.="<td>".(string)$data['kode_kab']."</td>";
						$newtbl.="<td>".(string)$data['kode_kec']."</td>";
						$newtbl.="<td>".(string)$data['kode_desa']."</td>";
						$newtbl.="<td>".$data['nbs']."</td>";
						$newtbl.="<td>".$data['nks']."</td>";
						$newtbl.="<td>".$data['no_urut_ruta']."</td>";
						$newtbl.="<td>".$data['nama_krt']."</td>";
						
						$newtbl.="<td>".$data['error']."</td>";
						$newtbl.="<td>".$data['konfirmasi']."</td>";
						$newtbl.="<td>".$data['flag']."</td>";
						$newtbl.="<td>".$data['keterangan']."</td>";
						
						
						$newtbl.="</tr>";
						$no++;
					}
				
		}
		else {
			$query=Yii::$app->db->createCommand(" SELECT * FROM terror WHERE flag=".$id."  and kode_kab='".$kab."'  order by kode_prop, kode_kab, kode_kec, kode_desa, nbs, no_urut_ruta");

				$result=$query->queryAll();  

				foreach($result as $data)
					{
						$newtbl.="<tr >";
						
						$newtbl.="<td>".$data['id_terror']."</td>";
						$newtbl.="<td>".$data['kode_prop']."</td>";
						$newtbl.="<td>".(string)$data['kode_kab']."</td>";
						$newtbl.="<td>".(string)$data['kode_kec']."</td>";
						$newtbl.="<td>".(string)$data['kode_desa']."</td>";
						$newtbl.="<td>".$data['nbs']."</td>";
						$newtbl.="<td>".$data['nks']."</td>";
						$newtbl.="<td>".$data['no_urut_ruta']."</td>";
						$newtbl.="<td>".$data['nama_krt']."</td>";
						
						$newtbl.="<td>".$data['error']."</td>";
						$newtbl.="<td>".$data['konfirmasi']."</td>";
						$newtbl.="<td>".$data['keterangan']."</td>";
						
						
						$newtbl.="</tr>";
						$no++;
					}
			}
			Yii::$app->db->createCommand("INSERT INTO tdownload (kode_kab, kode_tabel_download, user, islengkap, jumlah_baris, updated_at) VALUES ( '".$wil."' ,5,'".$user."' ,1, '".$no."' , NOW())")->execute();	
    }
    else if ($id==3){
		$newtbl="<table border=\"1\" width=\"100%\">";
					$newtbl.="<thead><tr >";
						$newtbl.="<th>ID</th>";
						$newtbl.="<th>Provinsi</th>";
						$newtbl.="<th>Kabupaten</th>";
						$newtbl.="<th>Kecamatan</th>";
						$newtbl.="<th>Desa</th>";
						$newtbl.="<th>NBS</th>";
						$newtbl.="<th>NKS</th>";
						$newtbl.="<th>NURT</th>";
						$newtbl.="<th>Nama KRT</th>";
						
						$newtbl.="<th>Error</th>";
						$newtbl.="<th>Konfirmasi</th>";
						$newtbl.="<th>Catatan</th>";
						$newtbl.="<th>Operator</th>";
						
						
										
					$newtbl.="</tr></thead>";
				$newtbl.="<tbody>";

		if ($kab=='00'){
			
				$query=Yii::$app->db->createCommand(" SELECT * FROM terror WHERE flag=".$id."  order by kode_prop, kode_kab, kode_kec, kode_desa, nbs, no_urut_ruta");

				$result=$query->queryAll();  

				foreach($result as $data)
					{
						$newtbl.="<tr >";
						
						$newtbl.="<td>".$data['id_terror']."</td>";
						$newtbl.="<td>".(string)$data['kode_prop']."</td>";
						$newtbl.="<td>".(string)$data['kode_kab']."</td>";
						$newtbl.="<td>".(string)$data['kode_kec']."</td>";
						$newtbl.="<td>".(string)$data['kode_desa']."</td>";
						$newtbl.="<td>".$data['nbs']."</td>";
						$newtbl.="<td>".$data['nks']."</td>";
						$newtbl.="<td>".$data['no_urut_ruta']."</td>";
						$newtbl.="<td>".$data['nama_krt']."</td>";
						
						$newtbl.="<td>".$data['error']."</td>";
						$newtbl.="<td>".$data['konfirmasi']."</td>";
						$newtbl.="<td>".$data['keterangan']."</td>";
						$newtbl.="<td>".$data['operator']."</td>";
						
						
						$newtbl.="</tr>";
						$no++;
					}
				
		}
		else {
			$query=Yii::$app->db->createCommand(" SELECT * FROM terror WHERE flag=".$id."  and kode_kab='".$kab."'  order by kode_prop, kode_kab, kode_kec, kode_desa, nbs, no_urut_ruta ");

				$result=$query->queryAll();  

				foreach($result as $data)
					{
						$newtbl.="<tr >";
						
						$newtbl.="<td>".$data['id_terror']."</td>";
						$newtbl.="<td>".$data['kode_prop']."</td>";
						$newtbl.="<td>".(string)$data['kode_kab']."</td>";
						$newtbl.="<td>".(string)$data['kode_kec']."</td>";
						$newtbl.="<td>".(string)$data['kode_desa']."</td>";
						$newtbl.="<td>".$data['nbs']."</td>";
						$newtbl.="<td>".$data['nks']."</td>";
						$newtbl.="<td>".$data['no_urut_ruta']."</td>";
						$newtbl.="<td>".$data['nama_krt']."</td>";
						
						$newtbl.="<td>".$data['error']."</td>";
						$newtbl.="<td>".$data['konfirmasi']."</td>";
						$newtbl.="<td>".$data['keterangan']."</td>";
						$newtbl.="<td>".$data['operator']."</td>";
						
						$newtbl.="</tr>";
						$no++;
					}
			}
		Yii::$app->db->createCommand("INSERT INTO tdownload (kode_kab, kode_tabel_download, user, islengkap, jumlah_baris, updated_at) VALUES ( '".$wil."' ,6,'".$user."' ,1, '".$no."' , NOW())")->execute();	
    }
	else if ($id==4){
		$newtbl="<table border=\"1\" width=\"100%\">";
					$newtbl.="<thead><tr >";
						$newtbl.="<th>ID</th>";
						$newtbl.="<th>Provinsi</th>";
						$newtbl.="<th>Kabupaten</th>";
						$newtbl.="<th>Kecamatan</th>";
						$newtbl.="<th>Desa</th>";
						$newtbl.="<th>NBS</th>";
						$newtbl.="<th>NKS</th>";
						$newtbl.="<th>NURT</th>";
						$newtbl.="<th>Nama KRT</th>";
						
						$newtbl.="<th>Error</th>";
						$newtbl.="<th>Konfirmasi</th>";
						$newtbl.="<th>Catatan</th>";						
						
						
										
					$newtbl.="</tr></thead>";
				$newtbl.="<tbody>";

		if ($kab=='00'){
			
				$query=Yii::$app->db->createCommand(" SELECT * FROM terror WHERE flag=4 or flag=5 or flag=6  order by kode_prop, kode_kab, kode_kec, kode_desa, nbs, no_urut_ruta");

				$result=$query->queryAll();  

				foreach($result as $data)
					{
						$newtbl.="<tr >";
						
						$newtbl.="<td>".$data['id_terror']."</td>";
						$newtbl.="<td>".(string)$data['kode_prop']."</td>";
						$newtbl.="<td>".(string)$data['kode_kab']."</td>";
						$newtbl.="<td>".(string)$data['kode_kec']."</td>";
						$newtbl.="<td>".(string)$data['kode_desa']."</td>";
						$newtbl.="<td>".$data['nbs']."</td>";
						$newtbl.="<td>".$data['nks']."</td>";
						$newtbl.="<td>".$data['no_urut_ruta']."</td>";
						$newtbl.="<td>".$data['nama_krt']."</td>";
						
						$newtbl.="<td>".$data['error']."</td>";
						$newtbl.="<td>".$data['konfirmasi']."</td>";
						$newtbl.="<td>".$data['keterangan']."</td>";
						
						
						$newtbl.="</tr>";
						$no++;
					}
				
		}
		else {
			$query=Yii::$app->db->createCommand(" SELECT * FROM terror WHERE flag=4 or flag=5 or flag=6 and kode_kab='".$kab."'  order by kode_prop, kode_kab, kode_kec, kode_desa, nbs, no_urut_ruta ");

				$result=$query->queryAll();  

				foreach($result as $data)
					{
						$newtbl.="<tr >";
						
						$newtbl.="<td>".$data['id_terror']."</td>";
						$newtbl.="<td>".$data['kode_prop']."</td>";
						$newtbl.="<td>".(string)$data['kode_kab']."</td>";
						$newtbl.="<td>".(string)$data['kode_kec']."</td>";
						$newtbl.="<td>".(string)$data['kode_desa']."</td>";
						$newtbl.="<td>".$data['nbs']."</td>";
						$newtbl.="<td>".$data['nks']."</td>";
						$newtbl.="<td>".$data['no_urut_ruta']."</td>";
						$newtbl.="<td>".$data['nama_krt']."</td>";
						
						$newtbl.="<td>".$data['error']."</td>";
						$newtbl.="<td>".$data['konfirmasi']."</td>";
						$newtbl.="<td>".$data['keterangan']."</td>";
						
						
						$newtbl.="</tr>";
						$no++;
					}
			}
		Yii::$app->db->createCommand("INSERT INTO tdownload (kode_kab, kode_tabel_download, user, islengkap, jumlah_baris, updated_at) VALUES ( '".$wil."' ,8,'".$user."' ,1, '".$no."' , NOW())")->execute();	
    }
		$newtbl.="</tbody></table>";

		echo $newtbl;
	}
	
    /**
     * Displays a single Terror model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Terror model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Terror();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_terror]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Terror model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_terror]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Terror model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Terror model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Terror the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Terror::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionUploaderror()
    {
		date_default_timezone_set("asia/jakarta");
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){
			$wil=$data['wilayah'];
			$user=$data['username'];}
		$sheetData[0][0]=0;
        $modelUpload= new \yii\base\DynamicModel(['fileUpload' => 'File Import',]);
		$modelUpload->addRule(['fileUpload'],'required');
		//$modelUpload->addRule(['fileUpload'],'file',['extensions'=>'csv,xlsx,xls'],['maxsize'=>1024*1024]);
		
		$test=1;
		if(Yii::$app->request->post()) {
			//$test=2;
			$modelUpload->fileUpload = \yii\web\UploadedFile::getInstance($modelUpload, 'fileUpload');
			if ($modelUpload->fileUpload && $modelUpload->validate()) {
				
				$test=3;
				$inputFileType = \PHPExcel_IOFactory::identify($modelUpload->fileUpload->tempName);
				$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($modelUpload->fileUpload->tempName);
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				$baseRow=2;
				//$test=$modelUpload->fileUpload->tempName;
				$no=1;
				
				while (!empty($sheetData[$baseRow]['A'])){
					$test=4;
					$model = new Terror();
					/* $randnum = rand(1111111111,9999999999);
					$model->id_terror=$randnum; */
					$model->kode_prop=(string)$sheetData[$baseRow]['A'];
					$model->kode_kab=(string)$sheetData[$baseRow]['B'];
					$model->kode_kec=(string)$sheetData[$baseRow]['C'];
					$model->kode_desa=(string)$sheetData[$baseRow]['D'];
					$model->nbs=(string)$sheetData[$baseRow]['E'];
					$model->nks=(string)$sheetData[$baseRow]['F'];
					$model->no_urut_ruta=(integer)$sheetData[$baseRow]['G'];
					//$model->nama_krt=(string)$sheetData[$baseRow]['O'];
					
					$model->nama_krt=preg_replace('/[^0-9a-zA-Z -]/','',(string)$sheetData[$baseRow]['H']);
					$model->error=addslashes((string)$sheetData[$baseRow]['I']);
					$model->flag=1;
					$model->created_at=date('Y-m-d H:i:s');
					$model->updated_at=date('Y-m-d H:i:s');
					$model->operator=(string)$sheetData[$baseRow]['J'];
					
					$no++;										
					
					$model->save();
					
					$baseRow++;
					}
				
				Yii::$app->session->setFlash('sukses','Data berhasil disimpan.');
				Yii::$app->db->createCommand("INSERT INTO tupload (kode_kab, kode_tabel_upload, user, islengkap, jumlah_baris, updated_at) VALUES ( '".$wil."' ,1,'".$user."' ,1, '".$no."' , NOW())")->execute();
			}
		else 
			 Yii::$app->session->setFlash('error','Data gagal tersimpan. Mohon cek kembali file yang anda masukkan');
		}		
		
		$searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		return $this->render('uploaderror', [
            'modelUpload' => $modelUpload,
			'test' => $test,
			//'test' => $sheetData,

        ]);
    }
	
	public function actionUploadkonfirmasi()
    {
	
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){
			$wil=$data['wilayah'];
			$user=$data['username'];}
		$sheetData[0][0]=0;
        $modelUpload= new \yii\base\DynamicModel(['fileUpload' => 'File Import',]);
		$modelUpload->addRule(['fileUpload'],'required');
		//$modelUpload->addRule(['fileUpload'],'file',['extensions'=>'csv,xlsx,xls'],['maxsize'=>1024*1024]);
		
		$test=1;
		if(Yii::$app->request->post()) {
			//$test=2;
			$modelUpload->fileUpload = \yii\web\UploadedFile::getInstance($modelUpload, 'fileUpload');
			if ($modelUpload->fileUpload && $modelUpload->validate()) {
				
				$test=3;
				$inputFileType = \PHPExcel_IOFactory::identify($modelUpload->fileUpload->tempName);
				$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($modelUpload->fileUpload->tempName);
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				$baseRow=2;
				//$test=$modelUpload->fileUpload->tempName;
				$no=0;
				
				while (!empty($sheetData[$baseRow]['A'])){
					$test=4;
					$model = new Terror();
					$model->id_terror=(string)$sheetData[$baseRow]['A'];
					/* $model->kode_prop=(string)$sheetData[$baseRow]['A'];
					$model->kode_kab=(string)$sheetData[$baseRow]['B'];
					$model->kode_kec=(string)$sheetData[$baseRow]['C'];
					$model->kode_desa=(string)$sheetData[$baseRow]['D'];
					$model->nbs=(string)$sheetData[$baseRow]['E'];
					$model->nks=(string)$sheetData[$baseRow]['F'];
					$model->no_urut_ruta=(integer)$sheetData[$baseRow]['G']; */
					//$model->nama_krt=(string)$sheetData[$baseRow]['O'];
					
					/* $model->nama_krt=preg_replace('/[^0-9a-zA-Z -]/','',(string)$sheetData[$baseRow]['H']);
					$model->error=(string)$sheetData[$baseRow]['I']; */
					$model->konfirmasi=addslashes((string)$sheetData[$baseRow]['K']);
					$model->keterangan=addslashes((string)$sheetData[$baseRow]['L']);
					
					if (!empty($sheetData[$baseRow]['K'])){
						Yii::$app->db->createCommand("UPDATE terror SET konfirmasi='".$model->konfirmasi."' , keterangan='".$model->keterangan."' , flag = 2 , updated_at=now() WHERE flag<>3 and id_terror= '".$model->id_terror."' ")->execute();
						$no++;
					}
										
					
					
					
					$baseRow++;
					}
				
				Yii::$app->session->setFlash('sukses','Data berhasil disimpan.');
				Yii::$app->db->createCommand("INSERT INTO tupload (kode_kab, kode_tabel_upload, user, islengkap, jumlah_baris, updated_at) VALUES ( '".$wil."' ,2,'".$user."' ,1, '".$no."' , NOW())")->execute();
			}
		else 
			 Yii::$app->session->setFlash('error','Data gagal tersimpan. Mohon cek kembali file yang anda masukkan');
		}		
		
		$searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		return $this->render('uploadkonfirmasi', [
            'modelUpload' => $modelUpload,
			'test' => $test,
			//'test' => $sheetData,

        ]);
    }
	
	public function actionUploadapproval()
    {
	
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){
			$wil=$data['wilayah'];
			$user=$data['username'];}
		$sheetData[0][0]=0;
        $modelUpload= new \yii\base\DynamicModel(['fileUpload' => 'File Import',]);
		$modelUpload->addRule(['fileUpload'],'required');
		//$modelUpload->addRule(['fileUpload'],'file',['extensions'=>'csv,xlsx,xls'],['maxsize'=>1024*1024]);
		
		$test=1;
		if(Yii::$app->request->post()) {
			//$test=2;
			$modelUpload->fileUpload = \yii\web\UploadedFile::getInstance($modelUpload, 'fileUpload');
			if ($modelUpload->fileUpload && $modelUpload->validate()) {
				
				$test=3;
				$inputFileType = \PHPExcel_IOFactory::identify($modelUpload->fileUpload->tempName);
				$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($modelUpload->fileUpload->tempName);
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				$baseRow=2;
				//$test=$modelUpload->fileUpload->tempName;
				$no=0;
				
				while (!empty($sheetData[$baseRow]['A'])){
					$test=4;
					$model = new Terror();
					$model->id_terror=(string)$sheetData[$baseRow]['A'];
					/* $model->kode_prop=(string)$sheetData[$baseRow]['A'];
					$model->kode_kab=(string)$sheetData[$baseRow]['B'];
					$model->kode_kec=(string)$sheetData[$baseRow]['C'];
					$model->kode_desa=(string)$sheetData[$baseRow]['D'];
					$model->nbs=(string)$sheetData[$baseRow]['E'];
					$model->nks=(string)$sheetData[$baseRow]['F'];
					$model->no_urut_ruta=(integer)$sheetData[$baseRow]['G']; */
					//$model->nama_krt=(string)$sheetData[$baseRow]['O'];
					
					/* $model->nama_krt=preg_replace('/[^0-9a-zA-Z -]/','',(string)$sheetData[$baseRow]['H']);
					$model->error=(string)$sheetData[$baseRow]['I']; */
					$model->flag=(string)$sheetData[$baseRow]['L'];
					$model->keterangan=addslashes((string)$sheetData[$baseRow]['M']);
					
					if (!empty($sheetData[$baseRow]['L'])){
						Yii::$app->db->createCommand("UPDATE terror SET flag='".$model->flag."' , keterangan='".$model->keterangan."' , updated_at=now()   WHERE flag<>1 and id_terror= '".$model->id_terror."' ")->execute();
						$no++;
					}
										
					
					
					
					$baseRow++;
					}
				
				Yii::$app->session->setFlash('sukses','Data berhasil disimpan.');
				Yii::$app->db->createCommand("INSERT INTO tupload (kode_kab, kode_tabel_upload, user, islengkap, jumlah_baris, updated_at) VALUES ( '".$wil."' ,3,'".$user."' ,1, '".$no."' , NOW())")->execute();
			}
		else 
			 Yii::$app->session->setFlash('error','Data gagal tersimpan. Mohon cek kembali file yang anda masukkan');
		}		
		
		$searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		return $this->render('uploadapproval', [
            'modelUpload' => $modelUpload,
			'test' => $test,
			//'test' => $sheetData,

        ]);
    }
	
	public function actionUploadsudahdiperbaiki()
    {
	
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){
			$wil=$data['wilayah'];
			$user=$data['username'];}
		$sheetData[0][0]=0;
        $modelUpload= new \yii\base\DynamicModel(['fileUpload' => 'File Import',]);
		$modelUpload->addRule(['fileUpload'],'required');
		//$modelUpload->addRule(['fileUpload'],'file',['extensions'=>'csv,xlsx,xls'],['maxsize'=>1024*1024]);
		
		$test=1;
		if(Yii::$app->request->post()) {
			//$test=2;
			$modelUpload->fileUpload = \yii\web\UploadedFile::getInstance($modelUpload, 'fileUpload');
			if ($modelUpload->fileUpload && $modelUpload->validate()) {
				
				$test=3;
				$inputFileType = \PHPExcel_IOFactory::identify($modelUpload->fileUpload->tempName);
				$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($modelUpload->fileUpload->tempName);
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				$baseRow=2;
				//$test=$modelUpload->fileUpload->tempName;
				$no=0;
				
				while (!empty($sheetData[$baseRow]['A'])){
					$test=4;
					$model = new Terror();
					$model->id_terror=(string)$sheetData[$baseRow]['A'];
					/* $model->kode_prop=(string)$sheetData[$baseRow]['A'];
					$model->kode_kab=(string)$sheetData[$baseRow]['B'];
					$model->kode_kec=(string)$sheetData[$baseRow]['C'];
					$model->kode_desa=(string)$sheetData[$baseRow]['D'];
					$model->nbs=(string)$sheetData[$baseRow]['E'];
					$model->nks=(string)$sheetData[$baseRow]['F'];
					$model->no_urut_ruta=(integer)$sheetData[$baseRow]['G']; */
					//$model->nama_krt=(string)$sheetData[$baseRow]['O'];
					
					/* $model->nama_krt=preg_replace('/[^0-9a-zA-Z -]/','',(string)$sheetData[$baseRow]['H']);
					$model->error=(string)$sheetData[$baseRow]['I']; */
					$model->flag=(string)$sheetData[$baseRow]['N'];
					
					if (!empty($sheetData[$baseRow]['A'])){
						Yii::$app->db->createCommand("UPDATE terror SET flag='".$model->flag."' , keterangan='".$model->keterangan."' , updated_at=now()  WHERE flag<>1 and id_terror= '".$model->id_terror."' ")->execute();
						$no++;
					}
										
					
					
					
					$baseRow++;
					}
				
				Yii::$app->session->setFlash('sukses','Data berhasil disimpan.');
				Yii::$app->db->createCommand("INSERT INTO tupload (kode_kab, kode_tabel_upload, user, islengkap, jumlah_baris, updated_at) VALUES ( '".$wil."' ,7,'".$user."' ,1, '".$no."' , NOW())")->execute();
			}
		else 
			 Yii::$app->session->setFlash('error','Data gagal tersimpan. Mohon cek kembali file yang anda masukkan');
		}		
		
		$searchModel = new TerrorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		return $this->render('uploadsudahdiperbaiki', [
            'modelUpload' => $modelUpload,
			'test' => $test,
			//'test' => $sheetData,

        ]);
    }
}
