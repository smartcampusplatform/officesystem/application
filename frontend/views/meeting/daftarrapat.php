<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\Controller;
use yii\grid\GridView;
use yii\web\Application;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Tuser */

$this->title = 'Daftar Rapat';
//$this->params['breadcrumbs'][] = ['label' => 'Thargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>	

<div class="box-body">
<div class="callout callout-success">
        <h4>Daftar Rapat</h4>

        <p>Daftar Rapat</p>
      </div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			
<div class="box-body">
 
<?php

$query=Yii::$app->db->createCommand("select * FROM meeting ");
$result=$query->queryAll();  
$no=1;

$newtbl="<table id=\"example1\" class=\"table table-bordered table-striped\">";
			$newtbl.="<thead><tr >";
				$newtbl.="<th>No Urut</th>";
				
				$newtbl.="<th>Nama Rapat</th>";
				$newtbl.="<th>Pemimpin Rapat</th>";			
				$newtbl.="<th>Tempat</th>";
				$newtbl.="<th>Tanggal</th>";
				$newtbl.="<th>Jam</th>";
				$newtbl.="<th>Jumlah Peserta</th>";				
				$newtbl.="<th></th>";					
								
			$newtbl.="</tr></thead>";
		$newtbl.="<tbody>";

foreach($result as $data)
	{
		$newtbl.="<tr >";
		$newtbl.="<td>".$no."</td>";
		$newtbl.="<td>".$data['name']."</td>";
		$newtbl.="<td>".$data['leader']."</td>";
		$newtbl.="<td>".$data['location']."</td>";
		$newtbl.="<td>".$data['date']."</td>";
		$newtbl.="<td>".$data['time']."</td>";
		$newtbl.="<td>".$data['attendee']."</td>";
		
		$newtbl.="<td>".Html::button('QR Code', ['value' => Url::to('index.php?r=meeting/viewqr'),'class' => 'btn btn-warning','id'=>'modalButton'])."</td>";
		
		
		$newtbl.="</tr>";
		$no++;
	}		
$newtbl.="</tbody></table>";

echo $newtbl;

	
	
?>
</div></div></div></div></div>
<?php
		Modal::begin([
			'header' => '<h4>QR Code</h4>',
			'id' => 'modal',
			'size' => 'modal-lg',
        ]); 
		echo "<div id='modalContent'><div>";
		Modal::end();
?>
<!-- jQuery 2.2.3 -->
<script src="<?php echo Yii::$app->request->baseUrl; ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- DataTables -->
<script src="<?php echo Yii::$app->request->baseUrl; ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl; ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>