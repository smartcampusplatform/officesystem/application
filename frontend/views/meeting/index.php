<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use amigos\QrcodeLibrary\QrCode;
use Endroid\QrCode\QrCode;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MeetingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meetings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Meeting', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_meeting',
            'name',
            'leader',
            'location',
            'attendee',
            //'date',
            //'time',
            //'notulen:ntext',
            //'notulis',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php 

$qrCode = (new QrCode('fahrur rozi'));
    //->setSize(250);
   // ->setMargin(5)
    //->useForegroundColor(51, 153, 255);

// now we can display the qrcode in many ways
// saving the result to a file:

$qrCode->writeFile(__DIR__ . '/code.png'); // writer defaults to PNG when none is specified

// display directly to the browser 
header('Content-Type: '.$qrCode->getContentType());
//echo $qrCode->writeString();

?> 

 <?php 
// or even as data:uri url
echo '<img src"' . $qrCode->writeDataUri() . '">';
echo "test";
?> 

</div>
