<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use Da\QrCode\QrCode;
/* use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url; */

/* @var $this yii\web\View */
/* @var $model app\models\Meeting */


?>

<div class="meeting-view">

<?php

$qrCode = (new QrCode('rapat'))
    ->setSize(250)
    ->setMargin(5)
    ->useForegroundColor(51, 153, 255);

// now we can display the qrcode in many ways
// saving the result to a file:

$qrCode->writeFile(__DIR__ . '/code.png'); // writer defaults to PNG when none is specified

// display directly to the browser 
//header('Content-Type: '.$qrCode->getContentType());
//echo $qrCode->writeString();

?> 

<?php 
// or even as data:uri url
//echo '<img src"' . $qrCode->writeDataUri() . '"/>';

echo '<img src="'.Yii::$app->request->baseUrl.'/views/meeting/code.png">';	

?>	
</div>