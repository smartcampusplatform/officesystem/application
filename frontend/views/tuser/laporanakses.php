<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\Controller;
use yii\grid\GridView;
use yii\web\Application;

/* @var $this yii\web\View */
/* @var $model app\models\Tuser */

$this->title = 'Laporan Akses';
//$this->params['breadcrumbs'][] = ['label' => 'Thargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>	

<div class="box-body">
<div class="callout callout-success">
        <h4>Laporan</h4>

        <p>Laporan akses website menurut user</p>
      </div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			
<div class="box-body">
<?php 

$query=Yii::$app->db->createCommand("select users.name as name, count(tuser.username) as jumlah,  max(tuser.updated_at) as last from tuser,users where tuser.username=users.username  and users.name != 'Administrator' group by tuser.username order by last DESC ");
$result=$query->queryAll();  
$no=1;

$newtbl="<table id=\"example1\" class=\"table table-bordered table-striped\">";
			$newtbl.="<thead><tr >";
				$newtbl.="<th>No Urut</th>";
				//$newtbl.="<th>User</th>";
				$newtbl.="<th>Nama</th>";
				$newtbl.="<th>Jumlah Akses</th>";			
				$newtbl.="<th>Akses Terakhir</th>";	
								
			$newtbl.="</tr></thead>";
		$newtbl.="<tbody>";

foreach($result as $data)
	{
		$newtbl.="<tr >";
		$newtbl.="<td>".$no."</td>";
		//$newtbl.="<td>".$data['username']."</td>";
		$newtbl.="<td>".$data['name']."</td>";
		$newtbl.="<td>".$data['jumlah']."</td>";
		$newtbl.="<td>".$data['last']."</td>";
		
		$newtbl.="</tr>";
		$no++;
	}		
$newtbl.="</tbody></table>";

echo $newtbl;
?>
</div></div></div></div></div>

<!-- jQuery 2.2.3 -->
<script src="<?php echo Yii::$app->request->baseUrl; ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- DataTables -->
<script src="<?php echo Yii::$app->request->baseUrl; ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl; ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>