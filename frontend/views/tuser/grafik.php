<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use linslin\yii2\curl;
use yii\web\Controller;
use yii\grid\GridView;
use app\models\Mkomoditas;
use app\models\Mkualitasmerk;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $model app\models\Tharga */

$this->title = 'Grafik Komoditas';
//$this->params['breadcrumbs'][] = ['label' => 'Thargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-body">
<div class="callout callout-info">
        <h4>Info</h4>

        <p>Grafik series data terbagi menjadi tiga yaitu perjam, perhari dan perbulan.</p>
      </div>
<?php 

$query=Yii::$app->db->createCommand("select * from mkomoditas where tampil=1 ");
$result=$query->queryAll(); 

		$form = ActiveForm::begin([
		 'action' => ['grafik'],
		 'method' => 'post',
		 ]); ?>
		 
          <div class="row">
			
            <div class="col-md-3">
				<?= $form->field($model, 'kd_komoditas')->dropDownList( 
								ArrayHelper::map($result, 'kd_komoditas','nm_komoditas'),
									['prompt' => 'Silakan pilih komoditas','maxlength' => true])->label(false);	  ?>
			</div>
			<div class="col-md-6">
					<?= Html::submitButton('Lihat',  ['class' => 'btn btn-success']) ?>
			</div>
		</div>
<?php ActiveForm::end(); ?>
<?php if (isset($nm_komoditas)) { ?>
	<div class="row">
        <!-- Left col -->
       <div class="col-md-12">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
			<li ><a href="#perjam-chart" data-toggle="tab">Perjam</a></li>
               <li ><a href="#perhari-chart" data-toggle="tab">Perhari</a></li>
              <li class="active" ><a href="#perbulan-chart" data-toggle="tab">Perbulan</a></li> 
              <li class="pull-left header"><i class="fa fa-inbox"></i> Harga Rata-rata </li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
			  <div class="chart tab-pane " id="perjam-chart" style=" height: 400px;">
			   <?php
				
				//echo $seriesperjam[0][0];
				
				echo	Highcharts::widget([
						'scripts' => [ 'modules/exporting',],			   
						'options' =>[
							'chart' =>[
										'type' => 'spline',
										'width' => 1050,
										],
							'title' =>['text' => 'Komoditas '.$nm_komoditas.' Per Jam'],
							'subtitle' =>['text' => 'Sumber : Data Ritel Online'],
							'xAxis' =>[
										'categories' => $jam,
										'opposite' => false,
										],
							'yAxis' =>[
										'title' =>['text' => 'Harga Rata-rata'],
										],
							'series' =>[
										['name' => $nm_komoditas, 'data' => $seriesperjam],
								],
							
							'plotOption' => [
									
									'column' => [
											'dataLabels' => [
												'enabled' => true,],
												],
											],
							'credits'=> false,
							],
						]
					);
					
					?>
			  </div>
			  <div class="chart tab-pane" id="perhari-chart" style=" height: 400px;">
			  <?php
				echo	Highcharts::widget([
						'scripts' => [ 'modules/exporting',],			   
						'options' =>[
							'chart' =>[
										'type' => 'spline',
										'width' => 1050,
										],
							'title' =>['text' => 'Komoditas '.$nm_komoditas.' Per Hari'],
							'subtitle' =>['text' => 'Sumber : Data Ritel Online'],
							'xAxis' =>['categories' => $tanggal],
							'yAxis' =>[
										'title' =>['text' => 'Harga Rata-rata'],
										],
							'series' =>[
										['name' =>  $nm_komoditas, 'data' => $seriespertanggal],
								],
							
							'plotOption' => [
									
									'column' => [
											'dataLabels' => [
												'enabled' => true,],
												],
											],
							'credits'=> false,
							],
						]
					);
					
					?>
			  
			  </div>
              <div class="chart tab-pane active" id="perbulan-chart" style=" height: 400px;">
			  <?php
				echo	Highcharts::widget([
						'scripts' => [ 'modules/exporting',],			   
						'options' =>[
							'chart' =>[
										'type' => 'spline',
										'width' => 1050,
										],
							'title' =>['text' => 'Komoditas '.$nm_komoditas.' Per Bulan'],
							'subtitle' =>['text' => 'Sumber : Data Ritel Online'],
							'xAxis' =>['categories' => $bulan],
							'yAxis' =>[
										'title' =>['text' => 'Harga Rata-rata'],
										],
							'series' =>[
										['name' => $nm_komoditas, 'data' => $seriesperbulan],
								],
							
							'plotOption' => [
									
									'column' => [
											'dataLabels' => [
												'enabled' => true,],
												],
											],
							'credits'=> false,
							],
						]
					);
					
					?>
			  </div>
              
            </div>
          </div>
		 </div>
	</div>
<?php }  
else 
	{echo "Tidak ada komoditas yang terpilih";} ?>

</div>


