<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tuser */

$this->title = 'Update Tuser: ' . $model->id_tuser;
$this->params['breadcrumbs'][] = ['label' => 'Tusers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tuser, 'url' => ['view', 'id' => $model->id_tuser]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tuser-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
