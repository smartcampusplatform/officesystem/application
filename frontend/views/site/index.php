<html>
<head>
<!-- refresh script setiap 60 detik -->
<meta http-equiv="refresh" content="60; url=<?php $_SERVER['PHP_SELF']; ?>"> 
</head>
<?php
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use evgeniyrru\yii2slick\Slick;
/* @var $this yii\web\View */

$this->title = 'Office';
?>
<?php 
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users WHERE id='.Yii::$app->user->getId().' '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){
			$user=$data['username'];			
			$role=$data['role'];			
			
					}
		$sqldata = Yii::$app->db->createCommand('SELECT * FROM users, employee WHERE users.id='.Yii::$app->user->getId().' AND employee.code_employee=users.kode_pegawai '); 
		$result = $sqldata->queryAll();
		foreach($result as $data){
			$id_employee=$data['id_employee'];			
			$name=$data['name'];			
					}
		if (!isset($test))
			$test=100;
		date_default_timezone_set('Asia/Jakarta');
?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="site-index">	
	 <!-- Main content -->
	 
    <section class="content">
	
 <!--	<div class="callout callout-success"> -->
 <!--        <h4>Selamat datang</h4> -->
		
	
  <!--  </div> -->
    <div class="callout callout-success">
        <h4>Selamat Datang, <?php echo $name; ?> </h4>
		
	
    </div>
	
<?php 
				$str = file_get_contents('http://localhost/smartoffice/services/public/api/destination/');
				$json = json_decode($str, true);

				$dok_masuk=0;
				
				foreach($json as $data)
					{													
						if ($data['receiver']==$id_employee)
							$dok_masuk++;
											
					}	
				
				$str = file_get_contents('http://localhost/smartoffice/services/public/api/document/');
				$json = json_decode($str, true);

				$dok_keluar=0;
				
				foreach($json as $data)
					{													
						if ($data['konseptor']==$id_employee)
							$dok_keluar++;
											
					}

				$str = file_get_contents('http://localhost/smartoffice/services/public/api/disposition/');
				$json = json_decode($str, true);

				$disposisi=0;
				
				foreach($json as $data)
					{													
						if ($data['receiver']==$id_employee)
							$disposisi++;
											
					}
				
				$str = file_get_contents('http://localhost/smartoffice/services/public/api/assignment/');
				$json = json_decode($str, true);

				$tugas=0;
				
				foreach($json as $data)
					{													
						if ($data['id_employee']==$id_employee)
							$tugas++;
											
					}
?>	
	<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $dok_masuk; ?></h3>

              <p>Dokumen Masuk</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-arrow-circle-down"></i>
            </div>
            <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $dok_keluar; ?></h3>

              <p>Dokumen Keluar</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-arrow-circle-up"></i>
            </div>
            <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $disposisi; ?></h3>

              <p>Disposisi</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-arrow-circle-right"></i>
            </div>
            <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
		
		<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php echo $tugas; ?></h3>

              <p>Tugas</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-file-text-o"></i>
            </div>
            <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       
        <!-- ./col -->
      </div>
	
	<div class="row">
	<!-- /.col -->
        <div class="col-lg-8">
		<div class="box box-solid box-success">
			<div class="box-header">
			  <h3 class="box-title">Progress Tugas Kondisi <?php echo date('Y-m-d H:i:s') ?></h3>
			</div><!-- /.box-header -->
			<div class="box-body">
		 
			<?php
			
				$str = file_get_contents('http://localhost/smartoffice/services/public/api/getprogress/');
				$json = json_decode($str, true);

				$nama=[];
				$persen=[];
				foreach($json as $data)
					{		
						if ($id_employee==$data['id_employee']){ 
						$nama[]=(string)$data['name'];
						$persen[]=(float)$data['realisation'];	}										
					}

				echo Slick::widget([
				'items' => [
					Highcharts::widget([
					'scripts' => [ 'modules/exporting', ],
					'options' => [
						'chart' => ['type' => 'column'],
						'title' => ['text' => 'Persentase (%) Tugas'],
						//'xAxis' => ['categories' => $nama,],
						'xAxis' => ['categories' => $nama ],
						'yAxis' => ['title' =>['text' => 'Jumlah',],],
						'series' =>	[
							//['name' => '', 'data' => $persen,'color'=>'green'],
							['name' => '', 'data' =>$persen,'color'=>'green'],
						],
						'plotOption' => [
							'column' => [
								'dataLabels' => [
									'enabled' => true,
									],
								],
							
							],
						'credits'=> false,
						],
			
					]),	 
					],

        // HTML attribute for every carousel item
        'itemOptions' => ['class' => 'cat-image'],

        // settings for js plugin
        // @see http://kenwheeler.github.io/slick/#settings
        'clientOptions' => [
            'autoplay' => true,
            'dots'     => true,
			 'speed'    => 1000,
			 'autoplaySpeed'    => 5000,
			 
			 //'breakpoint' => 1200,
            // note, that for params passing function you should use JsExpression object
            'onAfterChange' => new JsExpression('function() {console.log("The cat has shown")}'),
        ],

    ]); 
					
					?>				  
				
            
            <!-- /.box-body -->
			
				</div>
			</div>
		</div>
		  
		<div class="col-lg-4">
		<div class="box box-solid box-success">
			<div class="box-header">
			  <h3 class="box-title">Reminder Rapat</h3>
			</div><!-- /.box-header -->
			 <div class="box-body">
			<?php 			
				
				$str = file_get_contents('http://localhost/smartoffice/services/public/api/attendee/');
				$json = json_decode($str, true);
				$id_meeting=[];
				$no=0;
				foreach($json as $data)
					{	
						if ($id_employee==$data['id_employee']){
							
							$id_meeting[]=$data['id_meeting'];
							$no++;
							
						}
					}
					
				$str = file_get_contents('http://localhost/smartoffice/services/public/api/meeting/');
				$json = json_decode($str, true);

				$newtbl="<table width=\"100%\" id=\"example1\" class=\"table table-bordered table-striped\">";
						
					$newtbl.="<tbody>";

				foreach($json as $data)
					{	
						for ($i=0;$i<$no;$i++) {	
							if ($id_meeting[$i]==$data['id_meeting']){								
								$newtbl.="<tr >";					
								$newtbl.="<td width=\"100%\">
											<b><font size=\"2\">".$data['name']."</b></font>
											<br><font size=\"2\">Pemimpin Rapat : ".$data['leader']."</font>
											<br><font size=\"2\">Tempat : ".$data['location']."</font>
											<br><font size=\"2\">Tanggal : ".$data['date']." Pukul : ".$data['time']."</font></td>";									
								$newtbl.="</tr>";
							}
						}
					}

					
				$newtbl.="</tbody></table>";

				echo $newtbl;
				?>		
		  </div>
		   </div>
		    </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
	
	</section>	
</div>
      
 
</body>
</html>