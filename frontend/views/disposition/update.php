<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Disposition */

$this->title = 'Update Disposition: ' . $model->id_disposition;
$this->params['breadcrumbs'][] = ['label' => 'Dispositions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_disposition, 'url' => ['view', 'id' => $model->id_disposition]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="disposition-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
