<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_document') ?>

    <?= $form->field($model, 'no_document') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'sender') ?>

    <?= $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'content') ?>

    <?php // echo $form->field($model, 'id_signature') ?>

    <?php // echo $form->field($model, 'status')->checkbox() ?>

    <?php // echo $form->field($model, 'reviewer') ?>

    <?php // echo $form->field($model, 'konseptor') ?>

    <?php // echo $form->field($model, 'subject') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
