<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
	'modules' => [
		'gii' => 'yii\gii\Module',
	],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
			'parsers' => [
				'application/json' => 'yii\web\JsonParser',
			],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'identityCookie' => ['name' => '_identity-frontend', 
			'httpOnly' => true],
			
			
        ],
		
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
			'class' => 'yii\web\Session',
			'cookieParams' => ['httponly' => true, 'lifetime' => 3600 *4 ],
			'timeout' => 3600 * 4, //session expire
			'useCookies' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
              ],
            ],
        ],
        //'errorHandler' => [
           //'errorAction' => 'site/login',
       // ],
		'view' => [
            'theme' => [
               'pathMap' => [
                    '@app/views' => '@webroot/web/theme/yii2-app',                    
				],
			],
        ],
		'db'=>[
			'class' => '\yii\db\Connection',
			'dsn' => 'pgsql:host=127.0.0.1;dbname=neosmartoffice_db',
			'username' => 'postgres',
            'password' => 'fahrur27',
			
			'charset' => 'utf8',
		],
		/* 'assetManager' => [
			'bundles' => [
				'dmstr\web\AdminLteAsset' => [
					'skin' => 'skin-[green]',
				],
			],
		], */
		
		
        
        /* 'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
			
            'rules' => array(
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',),
        ], */
        
    ],
    'params' => $params,
	
];
