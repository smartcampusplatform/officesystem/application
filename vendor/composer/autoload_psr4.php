<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii'),
    'yii\\faker\\' => array($vendorDir . '/yiisoft/yii2-faker'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'rmrevin\\yii\\fontawesome\\' => array($vendorDir . '/rmrevin/yii2-fontawesome'),
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/reflection-docblock/src', $vendorDir . '/phpdocumentor/type-resolver/src'),
    'miloschuman\\highcharts\\' => array($vendorDir . '/miloschuman/yii2-highcharts-widget/src'),
    'linslin\\yii2\\curl\\' => array($vendorDir . '/linslin/yii2-curl'),
    'kartik\\sortable\\' => array($vendorDir . '/kartik-v/yii2-sortable'),
    'kartik\\select2\\' => array($vendorDir . '/kartik-v/yii2-widget-select2'),
    'kartik\\grid\\' => array($vendorDir . '/kartik-v/yii2-grid'),
    'kartik\\form\\' => array($vendorDir . '/kartik-v/yii2-widget-activeform'),
    'kartik\\export\\' => array($vendorDir . '/kartik-v/yii2-export'),
    'kartik\\dynagrid\\' => array($vendorDir . '/kartik-v/yii2-dynagrid'),
    'kartik\\dialog\\' => array($vendorDir . '/kartik-v/yii2-dialog'),
    'kartik\\base\\' => array($vendorDir . '/kartik-v/yii2-krajee-base'),
    'evgeniyrru\\yii2slick\\' => array($vendorDir . '/evgeniyrru/yii2-slick'),
    'dmstr\\' => array($vendorDir . '/dmstr/yii2-adminlte-asset'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'Zxing\\' => array($vendorDir . '/khanamiryan/qrcode-detector-decoder/lib'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\PropertyAccess\\' => array($vendorDir . '/symfony/property-access'),
    'Symfony\\Component\\OptionsResolver\\' => array($vendorDir . '/symfony/options-resolver'),
    'Symfony\\Component\\Inflector\\' => array($vendorDir . '/symfony/inflector'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\BrowserKit\\' => array($vendorDir . '/symfony/browser-kit'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'MyCLabs\\Enum\\' => array($vendorDir . '/myclabs/php-enum/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'Endroid\\QrCode\\' => array($vendorDir . '/endroid/qr-code/src'),
    'Endroid\\Installer\\' => array($vendorDir . '/endroid/installer/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Da\\QrCode\\' => array($vendorDir . '/2amigos/qrcode-library/src'),
    'Codeception\\Extension\\' => array($vendorDir . '/codeception/base/ext'),
    'Codeception\\' => array($vendorDir . '/codeception/base/src/Codeception'),
);
